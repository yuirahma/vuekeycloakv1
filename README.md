# vueKeycloakV1

get out of the bad mood

# vueapp

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### To add plugin vuetify
vue add vuetify

### To use router, jwt decode, keycloak.js, and vuex
npm install --save vue-router vue-jwt-decode keycloak.js vuex

