import Vue from 'vue';
import Vuex from 'vuex';
import KcAdminClient from '@keycloak/keycloak-admin-client';
import VueJwtDecode from 'vue-jwt-decode';

// import Keycloak from 'keycloak-js';

const initOptions = {
  baseUrl: 'http://34.236.66.38:8080/auth',
  realmName: 'RealmB',
};
// const keycloak = Keycloak({...initOptions,});
const kcAdminClient = new KcAdminClient({
  ...initOptions,
});
Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    kcAdminClient,
    tokens: {
      access_token: '',
      refresh_token: '',
    },
    userData: {
      name: '',
      username: '',
      email: '',
      roles: [],
    },
    allusers: [],
  },
  actions: {
    login: async (store, { username, password }) => {
      console.log(store, { username, password });
      await kcAdminClient.auth({
        clientId: 'vue-app',
        grantType: 'password',
        username,
        password,
      });
      const decoded = VueJwtDecode.decode(kcAdminClient.accessToken);
      console.log(kcAdminClient.accessToken);
      const name = decoded.name;
      const preferred_username = decoded.preferred_username;
      const roles = decoded.realm_access.roles;
      const email = decoded.realm_access.email;
      store.commit('store_name', name);
      store.commit('store_user', preferred_username);
      store.commit('store_roles', roles);
      store.commit('store_email', email);
      store.commit('storetokens', kcAdminClient.accessToken);
      store.commit('setAdminData', kcAdminClient);
      console.log(store.state);
    },
    retrieveUser: async (store) => {
      kcAdminClient.setAccessToken(store.state.tokens);
      const response = await store.state.kcAdminClient.users.find({});
      console.log(response);
    },
  },
  mutations: {
    store_name(state, data) {
      state.userData.name = data;
    },
    store_user(state, data) {
      state.userData.username = data;
    },
    store_email(state, data) {
      state.userData.email = data;
    },
    store_roles(state, data) {
      state.userData.roles = data;
    },
    storetokens(state, data) {
      state.tokens = data;
    },
    storeusers(state, data) {
      console.log(state.allusers);
      state.allusers = data;
    },
    setAdminData(state, data) {
      state.kcAdminClient = data;
    },
  },
});
